(define (domain test_domain)
  (:requirements :strips)
  (:predicates 
    (goal)
(can-do_dialogue-disambiguation-start_conversation)
(started)
    )

(:action dialogue-disambiguation-ask-for_break-pad 
:parameters () 
 :precondition (and (started)
 )
 :effect (and (labeled-oneof resolve-ask-for_break-pad (outcome ask-for_break-pad_help-local-options__ (and 
(not (can-do_dialogue-disambiguation-start_conversation)) ))
(outcome ask-for_break-pad_initiative-switch__ (and (can-do_dialogue-disambiguation-start_conversation)
 ))
(outcome ask-for_break-pad_detected__check-pass_status_options-eq-found (and 
(not (can-do_dialogue-disambiguation-start_conversation)) )) ) ))
(:action dialogue-disambiguation-start_conversation 
:parameters () 
 :precondition (and (can-do_dialogue-disambiguation-start_conversation)
 )
 :effect (and (labeled-oneof resolve-start_conversation (outcome start_conversation_break-pad__check-pass_status_options-eq-found (and (can-do_dialogue-disambiguation-start_conversation)
(started)
 ))
(outcome start_conversation_oil__check-oil_status-eq-found (and (can-do_dialogue-disambiguation-start_conversation)
(started)
 ))
(outcome start_conversation_spark-plug__check-pass_status_options-eq-found (and (can-do_dialogue-disambiguation-start_conversation)
(started)
 ))
(outcome start_conversation_clutch-seal-tightness__check-clutch_seal_tightness_status-eq-found (and (can-do_dialogue-disambiguation-start_conversation)
(started)
 ))
(outcome start_conversation_fallback__ (and (can-do_dialogue-disambiguation-start_conversation)
(started)
 ))
(outcome start_conversation_what__ (and (can-do_dialogue-disambiguation-start_conversation)
(started)
 ))
(outcome start_conversation_initiative-switch__ (and (can-do_dialogue-disambiguation-start_conversation)
(started)
 )) ) ))
(:action dialogue-disambiguation-ask-for_clutch-seal-tightness 
:parameters () 
 :precondition (and (started)
 )
 :effect (and (labeled-oneof resolve-ask-for_clutch-seal-tightness (outcome ask-for_clutch-seal-tightness_initiative-switch__ (and (can-do_dialogue-disambiguation-start_conversation)
 ))
(outcome ask-for_clutch-seal-tightness_detected__check-clutch_seal_tightness_status-eq-found (and 
(not (can-do_dialogue-disambiguation-start_conversation)) ))
(outcome ask-for_clutch-seal-tightness_help-local-options__ (and 
(not (can-do_dialogue-disambiguation-start_conversation)) )) ) ))
(:action dialogue-disambiguation-state-message 
:parameters () 
 :precondition (and (started)
 )
 :effect (and (labeled-oneof resolve-state-message (outcome state-message-outcome-fallback__ (and (can-do_dialogue-disambiguation-start_conversation)
 )) ) ))
(:action dialogue-disambiguation-ask-for_spark-plug 
:parameters () 
 :precondition (and (started)
 )
 :effect (and (labeled-oneof resolve-ask-for_spark-plug (outcome ask-for_spark-plug_initiative-switch__ (and (can-do_dialogue-disambiguation-start_conversation)
 ))
(outcome ask-for_spark-plug_help-local-options__ (and 
(not (can-do_dialogue-disambiguation-start_conversation)) ))
(outcome ask-for_spark-plug_detected__check-pass_status_options-eq-found (and 
(not (can-do_dialogue-disambiguation-start_conversation)) )) ) ))
(:action dialogue-disambiguation-ask-for_oil-level 
:parameters () 
 :precondition (and (started)
 )
 :effect (and (labeled-oneof resolve-ask-for_oil-level (outcome ask-for_oil-level_help-local-options__ (and 
(not (can-do_dialogue-disambiguation-start_conversation)) ))
(outcome ask-for_oil-level_initiative-switch__ (and (can-do_dialogue-disambiguation-start_conversation)
 ))
(outcome ask-for_oil-level_detected__check-oil_status-eq-found (and 
(not (can-do_dialogue-disambiguation-start_conversation)) )) ) ))
(:action dialogue-disambiguation-end_conversation 
:parameters () 
 :precondition (and (started)
 )
 :effect (and (labeled-oneof resolve-end_conversation (outcome end_conversation-outcome-fallback__ (and (goal)
(can-do_dialogue-disambiguation-start_conversation)
 )) ) ))

)
