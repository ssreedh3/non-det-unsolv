(define (problem test_problem)
 (:domain test_domain)

 (:objects )

 (:init 
    (can-do_dialogue-disambiguation-ask-for_oil-level)
(can-do_dialogue-disambiguation-ask-for_clutch-seal-tightness)
(can-do_dialogue-disambiguation-ask-for_break-pad)
(can-do_dialogue-disambiguation-start_conversation)
(user_initiative)
(can-do_dialogue-disambiguation-end_conversation)
(can-do_dialogue-disambiguation-ask-for_spark-plug)
(have_user_initiative_message)
(can-do_dialogue-disambiguation-state-message)
    )

 (:goal (and 
    (goal)
)
))
