(define (problem test_problem)
 (:domain test_domain)

 (:objects )

 (:init 
    (user_initiative)
(can-do_dialogue-disambiguation-ask-for_clutch-seal-tightness)
(can-do_dialogue-disambiguation-ask-for_break-pad)
(can-do_dialogue-disambiguation-ask-for_spark-plug)
(can-do_dialogue-disambiguation-end_conversation)
(can-do_dialogue-disambiguation-state-message)
(can-do_dialogue-disambiguation-start_conversation)
    )

 (:goal (and 
    (goal)
)
))
