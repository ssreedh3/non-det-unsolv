(define (problem test_problem)
 (:domain test_domain)

 (:objects )

 (:init 
    (can-do_dialogue-disambiguation-ask-for_clutch-seal-tightness)
(can-do_dialogue-disambiguation-ask-for_break-pad)
(can-do_dialogue-disambiguation-ask-for_spark-plug)
(can-do_dialogue-disambiguation-end_conversation)
(have_user_initiative_message)
(can-do_dialogue-disambiguation-ask-for_oil-level)
    )

 (:goal (and 
    (goal)
)
))
