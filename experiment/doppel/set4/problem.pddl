(define (problem test_problem)
 (:domain test_domain)

 (:objects )

 (:init 
    (can-do_cloud-function-determination-fetch-summary)
(can-do_dialogue-disambiguation__custom__confirm-title)
(can-do_dialogue-disambiguation-show-summary)
(can-do_dialogue-disambiguation-show-message)
(can-do_dialogue-disambiguation-ask-for-title)
(can-do_dialogue-disambiguation-ask-for-data)
(can-do_dialogue-disambiguation-refine-plot)
(can-do_logic-based-determination-reset)
(can-do_cloud-function-determination-generate-misha-plot)
(can-do_dialogue-disambiguation-agent-start)
    )

 (:goal (and 
    (goal)
)
))
