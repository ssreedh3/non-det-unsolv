(define
    (problem Bob-659ec0ca-00c3-5d29-4028-2dca3c489a64_problem)
    (:domain Bob-659ec0ca-00c3-5d29-4028-2dca3c489a64)
    (:objects
    )
    (:init
        (have_message)
        (can-do_logic-based-determination-decide_international_travel)
        (can-do_dialogue-disambiguation-start_conversation)
        (can-do_dialogue-disambiguation-end_conversation)
        (can-do_logic-based-determination-decide_domestic_travel)
        (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
        (can-do_dialogue-disambiguation-ask_if_user_has_license)
        (can-do_dialogue-disambiguation-ask_if_user_has_passport)
        (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
    )
    (:goal
        (and
            (GOAL)
        )
    )
)
