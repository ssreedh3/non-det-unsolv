(define
 (domain Bob-659ec0ca-00c3-5d29-4028-2dca3c489a64)
 (:requirements :strips :typing)
 (:types )
 (:constants
 
 )

 (:predicates 
 (have_message)
 (maybe-have_message)
 (have_name)
 (maybe-have_name)
 (confirm_has_passport)
 (confirm_has_license)
 (confirm_domestic_travel)
 (confirm_international_travel)
 (can_travel)
 (confirm_has_id)
 (asked_for_domestic)
 (asked_for_international)
 (GOAL)
 (STARTED)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 (:action logic-based-determination-decide_international_travel
 :parameters ()
 :precondition
 (and
 (can-do_logic-based-determination-decide_international_travel)
 (confirm_has_passport)
 (confirm_international_travel)
 (STARTED)
 )
 :effect
 (labeled-oneof outcome-decide_international_travel
 (outcome decide_international_travel-outcome-1
 (and
 (have_message)
 (not
 (maybe-have_message)
 )
 (not
 (can_travel)
 )
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome decide_international_travel-outcome-0
 (and
 (have_message)
 (not
 (maybe-have_message)
 )
 (can_travel)
 (not
 (can-do_logic-based-determination-decide_international_travel)
 )
 (not
 (can-do_dialogue-disambiguation-start_conversation)
 )
 (can-do_dialogue-disambiguation-end_conversation)
 (not
 (can-do_logic-based-determination-decide_domestic_travel)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
 )
 (:action dialogue-disambiguation-start_conversation
 :parameters ()
 :precondition
 (and
 (can-do_dialogue-disambiguation-start_conversation)
 (have_message)
 (not
 (maybe-have_message)
 )
 )
 :effect
 (labeled-oneof resolve-start_conversation
 (outcome start_conversation-outcome-1__check-name-eq-found
 (and
 (have_name)
 (not
 (maybe-have_name)
 )
 (have_name)
 (not
 (maybe-have_name)
 )
 (STARTED)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome start_conversation-outcome-fallback__
 (and
 (STARTED)
 (not
 (can-do_logic-based-determination-decide_international_travel)
 )
 (can-do_dialogue-disambiguation-start_conversation)
 (not
 (can-do_dialogue-disambiguation-end_conversation)
 )
 (not
 (can-do_logic-based-determination-decide_domestic_travel)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
 )
 (:action dialogue-disambiguation-end_conversation
 :parameters ()
 :precondition
 (and
 (can-do_dialogue-disambiguation-end_conversation)
 (can_travel)
 (STARTED)
 )
 :effect
 (labeled-oneof resolve-end_conversation
 (outcome end_conversation-outcome-fallback__
 (and
 (GOAL)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
 (:action logic-based-determination-decide_domestic_travel
 :parameters ()
 :precondition
 (and
 (can-do_logic-based-determination-decide_domestic_travel)
 (confirm_has_id)
 (confirm_domestic_travel)
 (STARTED)
 )
 :effect
 (labeled-oneof outcome-decide_domestic_travel
 (outcome decide_domestic_travel-outcome-0
 (and
 (have_message)
 (not
 (maybe-have_message)
 )
 (not
 (can-do_logic-based-determination-decide_international_travel)
 )
 (not
 (can-do_dialogue-disambiguation-start_conversation)
 )
 (can-do_dialogue-disambiguation-end_conversation)
 (not
 (can-do_logic-based-determination-decide_domestic_travel)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 )
 (not
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
 )
 (:action dialogue-disambiguation-ask_if_user_doing_domestic_travel
 :parameters ()
 :precondition
 (and
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (have_name)
 (not
 (maybe-have_name)
 )
 (not
 (asked_for_domestic)
 )
 (STARTED)
 )
 :effect
 (labeled-oneof resolve-ask_if_user_doing_domestic_travel
 (outcome ask_if_user_doing_domestic_travel-outcome-2__
 (and
 (not
 (confirm_domestic_travel)
 )
 (asked_for_domestic)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome ask_if_user_doing_domestic_travel-outcome-1__
 (and
 (confirm_domestic_travel)
 (asked_for_domestic)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome ask_if_user_doing_domestic_travel-outcome-fallback__
 (and
 (asked_for_domestic)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
 (:action dialogue-disambiguation-ask_if_user_has_license
 :parameters ()
 :precondition
 (and
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (not
 (confirm_has_id)
 )
 (STARTED)
 )
 :effect
 (labeled-oneof resolve-ask_if_user_has_license
 (outcome ask_if_user_has_license-outcome-2__
 (and
 (not
 (confirm_has_license)
 )
 (confirm_has_id)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome ask_if_user_has_license-outcome-1__
 (and
 (confirm_has_license)
 (confirm_has_id)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome ask_if_user_has_license-outcome-fallback__
 (and
 (confirm_has_id)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
 (:action dialogue-disambiguation-ask_if_user_has_passport
 :parameters ()
 :precondition
 (and
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (confirm_international_travel)
 (not
 (confirm_has_id)
 )
 (STARTED)
 )
 :effect
 (labeled-oneof resolve-ask_if_user_has_passport
 (outcome ask_if_user_has_passport-outcome-2__
 (and
 (not
 (confirm_has_passport)
 )
 (confirm_has_id)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome ask_if_user_has_passport-outcome-fallback__
 (and
 (confirm_has_id)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
 (:action dialogue-disambiguation-ask_if_user_is_doing_internation_travel
 :parameters ()
 :precondition
 (and
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 (have_name)
 (not
 (maybe-have_name)
 )
 (not
 (asked_for_international)
 )
 (STARTED)
 )
 :effect
 (labeled-oneof resolve-ask_if_user_is_doing_internation_travel
 (outcome ask_if_user_is_doing_internation_travel-outcome-2__
 (and
 (not
 (confirm_international_travel)
 )
 (asked_for_international)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome ask_if_user_is_doing_internation_travel-outcome-1__
 (and
 (confirm_international_travel)
 (asked_for_international)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 (outcome ask_if_user_is_doing_internation_travel-outcome-fallback__
 (and
 (asked_for_international)
 (can-do_logic-based-determination-decide_international_travel)
 (can-do_dialogue-disambiguation-start_conversation)
 (can-do_dialogue-disambiguation-end_conversation)
 (can-do_logic-based-determination-decide_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_doing_domestic_travel)
 (can-do_dialogue-disambiguation-ask_if_user_has_license)
 (can-do_dialogue-disambiguation-ask_if_user_has_passport)
 (can-do_dialogue-disambiguation-ask_if_user_is_doing_internation_travel)
 )
 )
 )
 )
)
