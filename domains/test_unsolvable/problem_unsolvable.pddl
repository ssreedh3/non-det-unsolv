(define
    (problem Car-Inspection-60f45608-0ec0-dbc9-1723-eb3820693d11_problem)
    (:domain Car-Inspection-60f45608-0ec0-dbc9-1723-eb3820693d11)
    (:objects
    )
    (:init
        (have_user_initiative_message)
        (can-do_dialogue-disambiguation-start_conversation)
        (can-do_dialogue-disambiguation-ask-for_oil-level)
        (can-do_dialogue-disambiguation-ask-for_clutch-seal-tightness)
        (can-do_dialogue-disambiguation-end_conversation)
        (can-do_dialogue-disambiguation-state-message)
        (can-do_dialogue-disambiguation-ask-for_break-pad)
    )
    (:goal
        (and
            (GOAL)
        )
    )
)