(define
    (problem Car-Inspection-2dd49c3a-2e2c-5554-fe99-37be7c247cbe_problem)
    (:domain Car-Inspection-2dd49c3a-2e2c-5554-fe99-37be7c247cbe)
    (:objects
    )
    (:init
        (have_user_initiative_message)
        (user_initiative)
        (can-do_dialogue-disambiguation-start_conversation)
        (can-do_dialogue-disambiguation-ask-for_oil-level)
        (can-do_dialogue-disambiguation-ask-for_clutch-seal-tightness)
        (can-do_dialogue-disambiguation-ask-for_break-pad)
        (can-do_dialogue-disambiguation-ask-for_spark-plug)
        (can-do_dialogue-disambiguation-end_conversation)
        (can-do_dialogue-disambiguation-state-message)
    )
    (:goal
        (and
            (GOAL)
        )
    )
)