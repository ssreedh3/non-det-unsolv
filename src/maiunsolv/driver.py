from maiunsolv.Abstraction import Abstractor
from maiunsolv.model_utils import read_model, write_model, make_non_det_domain, write_min_non_det_model
from maiunsolv.Utils import test_unsolvability, test_solvability, run_planner, landmark_generator, find_val_failure
from maiunsolv.Search import BFSSearch, UCSSearch
from maiunsolv.constants import *
import copy
import time
class Driver():
    def __init__(self, domain_file, problem_file, foil_file=None):
        # Read model obj
        if foil_file:
            with open(foil_file) as f_fd:
                self.foil = [ac.strip() for ac in f_fd.readlines()]
        else:
            self.foil = []
        self.model_obj, self.foil_fluent_set = read_model(domain_file, problem_file, self.foil)

        #assert test_unsolvability(self.model_obj), "problem is solvable"
        #write_model(self.model_obj, '/tmp/dom.pddl', '/tmp/prob.pddl')
        #exit(0)
        self.all_precs = copy.deepcopy(self.model_obj[DOMAIN][PREDICATES])

        self.cost_map = {}
        self.total_cost = 0
        self.get_all_cost_map()
        print ("Total Cost>>>", self.total_cost)
        #print (self.all_precs)
        exit(0)
        self.abs_obj = Abstractor(self.model_obj)

    def get_all_cost_map(self):
        for prec in self.all_precs:
            self.cost_map[prec] = 0
            if prec in self.model_obj[INSTANCE][INIT]:
                self.cost_map[prec] += 1
            if prec in self.model_obj[INSTANCE][GOAL]:
                self.cost_map[prec] += 1
            for act in self.model_obj[DOMAIN][ACTIONS]:
                if prec in self.model_obj[DOMAIN][ACTIONS][act][POS_PREC]:
                    self.cost_map[prec] += 1
                if prec in self.model_obj[DOMAIN][ACTIONS][act][NEG_PREC]:
                    self.cost_map[prec] += 1
                if prec in self.model_obj[DOMAIN][ACTIONS][act][ADDS]:
                    self.cost_map[prec] += 1
                if prec in self.model_obj[DOMAIN][ACTIONS][act][DELS]:
                    self.cost_map[prec] += 1
            self.total_cost += self.cost_map[prec]

    def get_set_size(self, prop_set):
        total_size = 0
        for prec in prop_set:
            total_size += self.cost_map[prec]
        return total_size
    def full_unsolvability_test(self):
        # Check if foil is given then is it solvable
        if len(self.foil) > 0 and test_solvability(self.model_obj):
            return False, None, None, None, None


        unsolvability_core, landmarks, goal_model = self.find_most_abstract_unsolvable_model()

        #if len(self.foil) > 0:
        #    test_plan = copy.deepcopy(self.foil)
        #else:
        print ("Looking for plausible plans")

        most_detailed_failure_node, test_plan = self.find_most_concrete_unsolvable_model(unsolvability_core)
        # Find failure information
        max_model_start = time.time()
        failure_info = find_val_failure(goal_model, test_plan)
        print ("Need to consider the fluents",unsolvability_core[0] - self.foil_fluent_set)
        print ("Any potential solution to the problem requires achieving", landmarks)
        print ("A possible test plan")
        print (test_plan)
        print ("failure>>",failure_info)
        return True, unsolvability_core[0] - self.foil_fluent_set, landmarks, test_plan, failure_info

    def find_most_abstract_unsolvable_model(self):
        start_state = [set(['goal'])|self.foil_fluent_set,[]]
        min_model_start = time.time()
        goal_state = BFSSearch(start_state, self.unsolvability_test, self.concretizer_succ)
        print ("Size of min abstraction in count>>", len(goal_state[0]))
        print ("Size of min abstraction>>", self.get_set_size(goal_state[0]))
        print("Time taken for min abstraction calculation>>>", time.time() - min_model_start)
        goal_model = self.abs_obj.get_abstract_model(goal_state[0])

        #goal_model = goal_state[0]
        # find landmarks
        landmark_generation_start = time.time()
        landmarks = landmark_generator(goal_model)
        if not landmarks:
            print ("Testing last fact")
            # Check if making the last fact true makes it solvable
            test_model = copy.deepcopy(goal_model)
            test_model[INSTANCE][INIT].add(goal_state[-1][-1])
            if test_solvability(test_model):
                landmarks = goal_state[-1][-1]
            else:
                # Else pass the negative
                landmarks = '(not '+ goal_state[-1][-1]+')'

        print("Time taken for landmark calculation>>>", time.time() - landmark_generation_start)

        non_det_model = make_non_det_domain(self.abs_obj.get_abstract_model(goal_state[0]))
        write_min_non_det_model(non_det_model, '/tmp/dom.pddl', '/tmp/prob.pddl')
        # Get plan fail info
        min_model_plan_start = time.time()
        plan = run_planner(self.abs_obj.get_abstract_model(goal_state[0] - set([goal_state[-1][-1]])))
        print("Time taken for min abstraction plan calculation>>>", time.time() - min_model_plan_start)
        print("abstraction plan size>>>", len(plan))
        return goal_state, landmarks, goal_model

    def concretizer_succ(self, curr_set):
        succ_list = []
        for prec in self.all_precs - curr_set[0]:
            succ_list.append([curr_set[0]|set([prec]), curr_set[1] +[prec]])
        return succ_list

    def abstractor_succ(self, curr_set):
        succ_list = []
        for prec in curr_set[0] - set(['goal']):
            succ_list.append([curr_set[0] - set([prec]), curr_set[1] +[prec]])
        return succ_list

    def unsolvability_test(self, curr_node):
        curr_set = curr_node[0]
        curr_model = self.abs_obj.get_abstract_model(curr_set)
        # Test if curr model is solvable
        return test_unsolvability(curr_model)

    def solvability_test(self, curr_node):
        curr_set = curr_node[0]
        curr_model = self.abs_obj.get_abstract_model(curr_set)
        # Test if curr model is solvable
        return test_solvability(curr_model)

    def find_most_concrete_unsolvable_model(self, unsolvability_core):
        start_state = [self.all_precs - (unsolvability_core[0] - (set(['goal'])|self.foil_fluent_set)) , []]
        max_model_start = time.time()
        goal_state = BFSSearch(start_state, self.solvability_test, self.abstractor_succ)
        print("Size of max abstraction approx>>", self.get_set_size(goal_state[0]))
        print("Size of max abstraction approx in count>>", len(goal_state[0]))
        print("Time taken for approx max abstraction calculation approx>>>", time.time() - max_model_start)
        max_model_plan_start = time.time()
        plan = run_planner(self.abs_obj.get_abstract_model(goal_state[0]))
        print("Time taken for max model approx plan calculation>>>", time.time() - max_model_plan_start)
        print("Max model approx plan size>>>", len(plan))

        start_state = [self.all_precs, []]
        max_model_start = time.time()
        goal_state = BFSSearch(start_state, self.solvability_test, self.abstractor_succ)
        print("Size of max abstraction>>", self.get_set_size(goal_state[0]))
        print("Size of max abstraction in count>>", len(goal_state[0]))
        print("Time taken for max abstraction calculation>>>", time.time() - max_model_start)
        max_model_plan_start = time.time()
        plan = run_planner(self.abs_obj.get_abstract_model(goal_state[0]))
        print("Time taken for max model plan calculation>>>", time.time() - max_model_plan_start)
        print("Max model plan size>>>", len(plan))
        return goal_state, plan


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 3:
        foil_file = sys.argv[3]
    else:
        foil_file = None

    dr = Driver(sys.argv[1], sys.argv[2], foil_file)
    dr.full_unsolvability_test()
    #print (model)