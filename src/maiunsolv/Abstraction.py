import copy
from maiunsolv.constants import *
class Abstractor():
    def __init__(self, original_model):
        self.original_model = original_model

    def get_abstract_model(self, props_tobe_included):
        new_model = {}
        new_model[INSTANCE] = {}
        # new_model[INSTANCE][INIT] = self.original_model[INSTANCE][INIT] & fluent_set

        # Add all conjunctions (because it has to be part of the abstraction)
        new_model[INSTANCE][INIT] = self.original_model[INSTANCE][INIT] & props_tobe_included

        new_model[INSTANCE][GOAL] = self.original_model[INSTANCE][GOAL] & props_tobe_included
        # self.original_model[INSTANCE][GOAL] & fluent_set

        new_model[DOMAIN] = {}
        new_model[DOMAIN][PREDICATES] = copy.deepcopy(props_tobe_included)
        new_model[DOMAIN][ACTIONS] = {}

        for act in self.original_model[DOMAIN][ACTIONS]:
            new_model[DOMAIN][ACTIONS][act] = {}
            for part in self.original_model[DOMAIN][ACTIONS][act]:
                if part == PARARMETERS:
                    new_model[DOMAIN][ACTIONS][act][part] = self.original_model[DOMAIN][ACTIONS][act][part]
                else:
                    new_model[DOMAIN][ACTIONS][act][part] = self.original_model[DOMAIN][ACTIONS][act][part] & props_tobe_included
        return new_model


