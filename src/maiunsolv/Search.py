from queue import Queue, PriorityQueue

def closed_list_key(node):
    return str(sorted(node[0]))

def BFSSearch(start_state, goal_test, succ_generator):
    fringe = Queue()
    closed = set()
    fringe.put(start_state)

    while not fringe.empty():

        node = fringe.get()  # [1]

        if node != None and closed_list_key(node) not in closed:
            goal_stat = goal_test(node)
            print (node[0], goal_stat)
            if goal_stat:
                return node  # .get_plan()
            closed.add(closed_list_key(node))
            successor_list = succ_generator(node)
            while successor_list:
                candidate_node = successor_list.pop()
                fringe.put(candidate_node)
    return None

def get_cost(pred_set, cost_map):
    total_cost = 0
    for pred in pred_set:
        total_cost += cost_map[pred]
    return total_cost

def UCSSearch(start_state, goal_test, succ_generator, cost_map):
    fringe = PriorityQueue()
    closed = set()
    fringe.put((0, start_state))

    while not fringe.empty():

        val, node = fringe.get()  # [1]

        if node != None and closed_list_key(node) not in closed:
            goal_stat = goal_test(node)
            print (node[0], goal_stat)
            if goal_stat:
                return node  # .get_plan()
            closed.add(closed_list_key(node))
            successor_list = succ_generator(node)
            while successor_list:
                candidate_node = successor_list.pop()
                fringe.put((get_cost(candidate_node[0], cost_map),candidate_node))
    return None
