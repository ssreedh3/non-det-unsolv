prec_missing="has an unsatisfied"
# path to fast downward #
VAL_PATH=$(locate VAL/validate|head -n 1)

# validate plan given domain and problem
output=$(${VAL_PATH} -v $1 $2 $3 | grep "${prec_missing}"|wc -l)
if [ ${output} -gt 0 ];
then
      failed_action=$(${VAL_PATH} -v $1 $2 $3 | grep "${prec_missing}"|awk -F "${prec_missing}" '{print $1}')
      actual_pred=`${VAL_PATH} -v $1 $2 $3 | grep -A 1 "${prec_missing}"|tail -n 1|sed 's/(Set .*(/(/'|sed 's/).*)/)/'`
      echo "Action ${failed_action} failed because the precondition ${actual_pred} was not met"
else
     echo "Error!"
fi
