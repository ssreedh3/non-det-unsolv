from maiunsolv.constants import *
from maiunsolv.Utils import test_unsolvability
from maiunsolv.model_utils import read_model, write_min_non_det_model, make_non_det_domain
import sys
import os
import copy
import numpy as np
from itertools import combinations

domain_file = sys.argv[1]
problem_file = sys.argv[2]
del_count = int(sys.argv[3])
file_location = sys.argv[4]

PROP_SEPARATOR = "#"

def make_propositions(model_dict):
    props_set = set()
    for act in model_dict[DOMAIN][ACTIONS]:
        for eff in model_dict[DOMAIN][ACTIONS][act][ADDS]:
            if 'goal' not in eff.lower():
                props_set.add(act + PROP_SEPARATOR + ADDS + PROP_SEPARATOR + eff)
    for prop in model_dict[INSTANCE][INIT]:
        props_set.add(INSTANCE + PROP_SEPARATOR + INIT + PROP_SEPARATOR + prop)
    return props_set

def update_the_model(model_dict, props):
    new_model = copy.deepcopy(model_dict)
    for prop in props:
        prop_parts = prop.split(PROP_SEPARATOR)
        if prop_parts[0] == INSTANCE:
            new_model[INSTANCE][INIT].remove(prop_parts[-1])
        else:
            new_model[DOMAIN][ACTIONS][prop_parts[0]][ADDS].remove(prop_parts[-1])
    return new_model
model_dict, foil = read_model(domain_file, problem_file, [])
prop_set = make_propositions(model_dict)

itr_id = 0
NO_MODELS_FOUND = 5
number_of_unsolv_found = 0
model_dict_list = []



#while number_of_unsolv_found < NO_MODELS_FOUND and itr_id < 300:
#all_props = [list(i) for i in combinations(list(prop_set)[: int(len(prop_set)/2)], del_count)]
all_props = [list(i) for i in combinations(list(prop_set), del_count)]
np.random.shuffle(all_props)
for props_to_remove in all_props:
    itr_id += 1
    print("Number of unsolvable models found", number_of_unsolv_found)
    if number_of_unsolv_found > NO_MODELS_FOUND - 1:
        break
    #prop_list = list(prop_set)
    #np.random.shuffle(prop_list)
    #props_to_remove_list = prop_list[:del_count]
    #props_to_remove = set(props_to_remove_list)
    new_model = update_the_model(model_dict, props_to_remove)
    if test_unsolvability(new_model):
        number_of_unsolv_found += 1
        model_dict_list.append(new_model)
        #prop_set -= set(props_to_remove)

assert number_of_unsolv_found == NO_MODELS_FOUND, "Didn't find ALL"
#exit(0)

SET_DIR_NAME = "set"

for ind in [2]:
    dir_name = SET_DIR_NAME + str(ind)
    new_path = os.path.join(file_location, dir_name)
    os.mkdir(new_path)
    domain_name = os.path.join(new_path, "domain.pddl")
    problem_name = os.path.join(new_path, "problem.pddl")
    write_min_non_det_model(make_non_det_domain(model_dict_list[ind]), domain_name, problem_name)
