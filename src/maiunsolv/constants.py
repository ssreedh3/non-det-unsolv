import os

MODEL_YAML = "model.yaml"

# Keys for model map
DOMAIN = "domain"
DOMAIN_NAME = "domain_test"
ACTIONS = "actions"
PREDICATES = "predicates"
POS_PREC = "prec"
NEG_PREC = "neg_prec"
LABELED_ONEOF = "labeled-oneof"
ADDS = "adds"
DELS = "dels"
PARARMETERS = "params"


INSTANCE = "instance"
INIT = "init"
GOAL = "goal"


SRC_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(SRC_DIR,'DATA/')
DOMAIN_TEMPLATE_FILE = os.path.join(DATA_DIR,"domain_template.pddl")
PROBLEM_TEMPLATE_FILE = os.path.join(DATA_DIR,"prob_template.pddl")

#UNDETERMINIZE
ONEOFF = "_DETDUP_"
OUTCOME = "-EQ-"


#ACHIEVE lndm action
ACHIEVE_ACT_PREFIX = "act_achieve_lndm_"
ACHIEVE_PROP_PREFIX = "achieve_lndm_"


DUMMY_LANDMARK = "dummy"


# Foil things
GENERATED_OBS_PREFIX = "FOIL_ACT_EXEC_"