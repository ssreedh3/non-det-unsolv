import re
import os
from queue import Queue
import copy
import tempfile
from maiunsolv.model_utils import write_model
from maiunsolv.constants import *

PLANNING_CMD = "./run_planner.sh {} {}"
LANDMARK_CMD = "./get_landmark.sh {} {} {}"
VAL_INFO_COMMAND = './val_information.sh {} {} {}'

def parse_land_line(orig_line, relative = False):
    if 'conj' in orig_line and 'NegatedAtom'  in orig_line:
        print ("Not modelled")
        exit(1)

    line = re.sub('^LM \d Atom','StAtom',orig_line)
    #line = re.sub('^LM \d NegatedAtom','StngAtom',orig_line)
    PARSE_STR = "Atom "
    CONJ_STR = "CONJ"
    if 'conj {Atom ' not in line:
        raw_atom = line.split(PARSE_STR)[-1].split(' (')[0].replace('(',' ').replace(', ',' ').replace(',',' ').replace(')','').strip()
        if "NegatedAtom" in line:
            atom = "not ("+raw_atom+")"
        else:
            atom = raw_atom
    else:
        print ("Doesn't support conjunctions")
        exit(1)

    ld_type = "curr"
    if relative:
        if 'gn ' in line:
            ld_type = "gn"
        else:
            ld_type = "nat"
    return (atom, ld_type)

def parse_landmark_file(lfile):
    with open(lfile) as l_fd:
        land_content = [re.sub(r'var\d+\(\d+\)->\d+', '', l.strip()) for l in l_fd.readlines()]

    if land_content == [""]:
        return {}
    ind = 0
    parent_map = {}
    while ind < len(land_content):
        new_land = []
        curr_atom, ld_type = parse_land_line(land_content[ind])
        if curr_atom not in parent_map and DUMMY_LANDMARK not in curr_atom:
            parent_map[curr_atom] = {'gn':set(), 'nat':set()}
        ind += 1
        while ind < len(land_content) and ('<-' in land_content[ind] or '->' in land_content[ind]):
            #print (land_content[ind])
            next_atom, order_type = parse_land_line(land_content[ind], relative=True)
            if '<-' in land_content[ind]:
                parent_map[curr_atom][order_type].add(next_atom)
            ind+=1

    return parent_map


def run_planner(model):
    fd, problem_file = tempfile.mkstemp()
    os.close(fd)
    fd, domain_file = tempfile.mkstemp()
    os.close(fd)
    write_model(model, domain_file, problem_file)

    # Run the planner
    plan = [act.strip() for act in os.popen(PLANNING_CMD.format(domain_file, problem_file)).read().split()]

    os.remove(domain_file)
    os.remove(problem_file)
    return plan

def test_unsolvability(model):
    plan = run_planner(model)
    if len(plan) > 0:
        return False
    else:
        if model[INSTANCE][GOAL] <= model[INSTANCE][INIT]:
            return False
        else:
            return True

def test_solvability(model):
    plan = run_planner(model)
    if len(plan) > 0:
        return True
    else:
        if model[INSTANCE][GOAL] <= model[INSTANCE][INIT]:
            return True
        else:
            return False

def run_landmark_gen_cmd(model):
    DEBUG = False
    if not DEBUG:
        fd, problem_file = tempfile.mkstemp()
        os.close(fd)
        fd, domain_file = tempfile.mkstemp()
        os.close(fd)
        fd, landmark_file = tempfile.mkstemp()
        os.close(fd)
    else:
        problem_file = "/tmp/prob"
        domain_file = "/tmp/dom"
        landmark_file = "/tmp/land"

    write_model(model, domain_file, problem_file)

    # Run the planner
    landmarks_status = [act.strip() for act in os.popen(LANDMARK_CMD.format(domain_file, problem_file, landmark_file)).read().split()]

    landmarks = parse_landmark_file(landmark_file)

    print ("LANDMARKS >>", landmarks)
    #exit()
    if not DEBUG:
        os.remove(domain_file)
        os.remove(problem_file)
        os.remove(landmark_file)
    return landmarks

def landmark_generator(model):
    # check if there is landmarks in the current model
    curr_landmarks_map =  run_landmark_gen_cmd(model)
    if len(curr_landmarks_map) > 0:
        return find_failure_landmarks(model, curr_landmarks_map)
    else:
        # if not return None
        return None

def compile_landmark_test_model(model, landmark_predc_map):
    #Todo: Using just NAT reachability to simplify the flow
    # Also ignoring the ordering between predc
    # Also assuming fact landmarks
    compiled_model = copy.deepcopy(model)
    new_props = set()
    for lnd in landmark_predc_map:
        compiled_model[DOMAIN][ACHIEVE_ACT_PREFIX+lnd] ={}
        compiled_model[DOMAIN][ACHIEVE_ACT_PREFIX + lnd][POS_PREC] = set()
        compiled_model[DOMAIN][ACHIEVE_ACT_PREFIX + lnd][ADDS] = set()
        compiled_model[DOMAIN][ACHIEVE_ACT_PREFIX + lnd][DELS] = set()
        compiled_model[DOMAIN][ACHIEVE_ACT_PREFIX + lnd][POS_PREC].add(lnd)
        new_props.add()
        for predc_lndm in landmark_predc_map[lnd]:
            compiled_model[DOMAIN][ACHIEVE_ACT_PREFIX + lnd][POS_PREC].add(ACHIEVE_PROP_PREFIX+predc_lndm)
            new_props.add(ACHIEVE_PROP_PREFIX + predc_lndm)
        compiled_model[DOMAIN][ACHIEVE_ACT_PREFIX + lnd][ADDS].add(ACHIEVE_PROP_PREFIX+lnd)
        new_props.add(ACHIEVE_PROP_PREFIX+lnd)
    return compiled_model

def get_ordered_landmarks(landmark_predc_map):
    # Perform topological sort
    # Following Kahn's algorithm
    curr_version_landmark_predc_map = copy.deepcopy(landmark_predc_map)
    no_incomin_edges = Queue()
    for lnd in landmark_predc_map:
        if len(landmark_predc_map[lnd]) == 0:
            no_incomin_edges.push(lnd)
    sorted_lnds = []
    while not no_incomin_edges.empty():
        node = no_incomin_edges.get()
        sorted_lnds.append(node)
        for other_node in curr_version_landmark_predc_map:
            if node in curr_version_landmark_predc_map[other_node]:
                curr_version_landmark_predc_map[other_node].remove(node)
                if len(curr_version_landmark_predc_map[other_node]) == 0:
                    no_incomin_edges.put(other_node)

    return sorted_lnds

def find_failure_landmarks(model, landmark_predc_map):
    compiled_model = compile_landmark_test_model(model, landmark_predc_map)
    ordered_landmark_list = get_ordered_landmarks(landmark_predc_map)
    for landmark in ordered_landmark_list:
        compiled_model[INSTANCE][GOAL] = set([ACHIEVE_PROP_PREFIX+landmark])
        if test_unsolvability(compiled_model):
            return landmark
    return None

def find_val_failure(model, plan):
    DEBUG = True
    if not DEBUG:
        fd, problem_file = tempfile.mkstemp()
        fd, domain_file = tempfile.mkstemp()
        fd, plan_file = tempfile.mkstemp()
    else:
        problem_file = "/tmp/prob"
        domain_file = "/tmp/dom"
        plan_file = "/tmp/plan"

    write_model(model, domain_file, problem_file)
    with open(plan_file,'w') as pfd:
        pfd.write("\n".join(['('+act+')' for act in plan]))

    # Run val
    output = [i.strip() for i in os.popen(VAL_INFO_COMMAND.format(domain_file, problem_file, plan_file)).read().strip().split('\n')]

    return output
