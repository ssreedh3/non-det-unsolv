# Keys for model map
DOMAIN = "domain"
DOMAIN_NAME = "domain_test"
ACTIONS = "actions"
POS_PREC = "prec"
NEG_PREC = "neg_prec"
LABELED_ONEOF = "labeled-oneof"
ADDS = "adds"
DELS = "dels"
PARAMETERS = "params"

PREDICATES = "predicates"

INSTANCE = "instance"
INIT = "init"
GOAL = "goal"