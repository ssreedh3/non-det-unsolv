begin_version
3
end_version
begin_metric
0
end_metric
15
begin_variable
var0
-1
2
Atom have_break_pad_status()
NegatedAtom have_break_pad_status()
end_variable
begin_variable
var1
-1
2
Atom have_clutch_seal_tightness_status()
NegatedAtom have_clutch_seal_tightness_status()
end_variable
begin_variable
var2
-1
2
Atom have_oil_status()
NegatedAtom have_oil_status()
end_variable
begin_variable
var3
-1
2
Atom have_spark_plug_status()
NegatedAtom have_spark_plug_status()
end_variable
begin_variable
var4
-1
2
Atom user_initiative()
NegatedAtom user_initiative()
end_variable
begin_variable
var5
-1
2
Atom started()
NegatedAtom started()
end_variable
begin_variable
var6
-1
2
Atom have_message()
NegatedAtom have_message()
end_variable
begin_variable
var7
-1
2
Atom can-do_dialogue-disambiguation-start_conversation()
NegatedAtom can-do_dialogue-disambiguation-start_conversation()
end_variable
begin_variable
var8
-1
2
Atom can-do_dialogue-disambiguation-ask-for_break-pad()
NegatedAtom can-do_dialogue-disambiguation-ask-for_break-pad()
end_variable
begin_variable
var9
-1
2
Atom can-do_dialogue-disambiguation-ask-for_clutch-seal-tightness()
NegatedAtom can-do_dialogue-disambiguation-ask-for_clutch-seal-tightness()
end_variable
begin_variable
var10
-1
2
Atom can-do_dialogue-disambiguation-ask-for_oil-level()
NegatedAtom can-do_dialogue-disambiguation-ask-for_oil-level()
end_variable
begin_variable
var11
-1
2
Atom can-do_dialogue-disambiguation-ask-for_spark-plug()
NegatedAtom can-do_dialogue-disambiguation-ask-for_spark-plug()
end_variable
begin_variable
var12
-1
2
Atom can-do_dialogue-disambiguation-end_conversation()
NegatedAtom can-do_dialogue-disambiguation-end_conversation()
end_variable
begin_variable
var13
-1
2
Atom can-do_dialogue-disambiguation-state-message()
NegatedAtom can-do_dialogue-disambiguation-state-message()
end_variable
begin_variable
var14
-1
2
Atom goal()
NegatedAtom goal()
end_variable
0
begin_state
1
1
1
1
0
1
1
0
0
0
0
0
0
0
1
end_state
begin_goal
1
14 0
end_goal
21
begin_operator
dialogue-disambiguation-ask-for_break-pad_DETDUP_resolve-ask-for_break-pad-EQ-ask-for_break-pad_detected__check-pass_status_options-eq-found 
2
5 0
4 1
9
0 8 0 1
0 9 -1 1
0 10 -1 1
0 11 -1 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 0 1 0
0 6 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_break-pad_DETDUP_resolve-ask-for_break-pad-EQ-ask-for_break-pad_help-local-options__ 
3
0 1
5 0
4 1
8
0 8 0 1
0 9 -1 1
0 10 -1 1
0 11 -1 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 6 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_break-pad_DETDUP_resolve-ask-for_break-pad-EQ-ask-for_break-pad_initiative-switch__ 
3
8 0
0 1
5 0
7
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 7 -1 0
0 13 -1 0
0 4 1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_clutch-seal-tightness_DETDUP_resolve-ask-for_clutch-seal-tightness-EQ-ask-for_clutch-seal-tightness_detected__check-clutch_seal_tightness_status-eq-found 
2
5 0
4 1
9
0 8 -1 1
0 9 0 1
0 10 -1 1
0 11 -1 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 1 1 0
0 6 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_clutch-seal-tightness_DETDUP_resolve-ask-for_clutch-seal-tightness-EQ-ask-for_clutch-seal-tightness_help-local-options__ 
3
1 1
5 0
4 1
8
0 8 -1 1
0 9 0 1
0 10 -1 1
0 11 -1 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 6 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_clutch-seal-tightness_DETDUP_resolve-ask-for_clutch-seal-tightness-EQ-ask-for_clutch-seal-tightness_initiative-switch__ 
3
9 0
1 1
5 0
7
0 8 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 7 -1 0
0 13 -1 0
0 4 1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_oil-level_DETDUP_resolve-ask-for_oil-level-EQ-ask-for_oil-level_detected__check-oil_status-eq-found 
2
5 0
4 1
9
0 8 -1 1
0 9 -1 1
0 10 0 1
0 11 -1 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 6 -1 0
0 2 1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_oil-level_DETDUP_resolve-ask-for_oil-level-EQ-ask-for_oil-level_help-local-options__ 
3
2 1
5 0
4 1
8
0 8 -1 1
0 9 -1 1
0 10 0 1
0 11 -1 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 6 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_oil-level_DETDUP_resolve-ask-for_oil-level-EQ-ask-for_oil-level_initiative-switch__ 
3
10 0
2 1
5 0
7
0 8 -1 0
0 9 -1 0
0 11 -1 0
0 12 -1 0
0 7 -1 0
0 13 -1 0
0 4 1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_spark-plug_DETDUP_resolve-ask-for_spark-plug-EQ-ask-for_spark-plug_detected__check-pass_status_options-eq-found 
2
5 0
4 1
9
0 8 -1 1
0 9 -1 1
0 10 -1 1
0 11 0 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 6 -1 0
0 3 1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_spark-plug_DETDUP_resolve-ask-for_spark-plug-EQ-ask-for_spark-plug_help-local-options__ 
3
3 1
5 0
4 1
8
0 8 -1 1
0 9 -1 1
0 10 -1 1
0 11 0 1
0 12 -1 1
0 7 -1 1
0 13 -1 0
0 6 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-ask-for_spark-plug_DETDUP_resolve-ask-for_spark-plug-EQ-ask-for_spark-plug_initiative-switch__ 
3
11 0
3 1
5 0
7
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 12 -1 0
0 7 -1 0
0 13 -1 0
0 4 1 0
1
end_operator
begin_operator
dialogue-disambiguation-end_conversation_DETDUP_resolve-end_conversation-EQ-end_conversation-outcome-fallback__ 
6
12 0
0 0
1 0
2 0
3 0
5 0
7
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 7 -1 0
0 13 -1 0
0 14 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-start_conversation_DETDUP_resolve-start_conversation-EQ-start_conversation_break-pad__check-pass_status_options-eq-found 
2
7 0
4 0
8
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 13 -1 0
0 0 -1 0
0 5 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-start_conversation_DETDUP_resolve-start_conversation-EQ-start_conversation_clutch-seal-tightness__check-clutch_seal_tightness_status-eq-found 
2
7 0
4 0
8
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 13 -1 0
0 1 -1 0
0 5 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-start_conversation_DETDUP_resolve-start_conversation-EQ-start_conversation_fallback__ 
2
7 0
4 0
7
0 8 -1 1
0 9 -1 1
0 10 -1 1
0 11 -1 1
0 12 -1 1
0 13 -1 1
0 5 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-start_conversation_DETDUP_resolve-start_conversation-EQ-start_conversation_initiative-switch__ 
1
7 0
8
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 13 -1 0
0 5 -1 0
0 4 0 1
1
end_operator
begin_operator
dialogue-disambiguation-start_conversation_DETDUP_resolve-start_conversation-EQ-start_conversation_oil__check-oil_status-eq-found 
2
7 0
4 0
8
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 13 -1 0
0 2 -1 0
0 5 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-start_conversation_DETDUP_resolve-start_conversation-EQ-start_conversation_spark-plug__check-pass_status_options-eq-found 
2
7 0
4 0
8
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 13 -1 0
0 3 -1 0
0 5 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-start_conversation_DETDUP_resolve-start_conversation-EQ-start_conversation_what__ 
2
7 0
4 0
7
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 13 -1 0
0 5 -1 0
1
end_operator
begin_operator
dialogue-disambiguation-state-message_DETDUP_resolve-state-message-EQ-state-message-outcome-fallback__ 
2
13 0
5 0
7
0 8 -1 0
0 9 -1 0
0 10 -1 0
0 11 -1 0
0 12 -1 0
0 7 -1 0
0 6 0 1
1
end_operator
0
