#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys

def python_version_supported():
    major, minor = sys.version_info[:2]
    return (major == 2 and minor >= 7) or (major, minor) >= (3, 2)

if not python_version_supported():
    sys.exit("Error: Translator only supports Python >= 2.7 and Python >= 3.2.")


from collections import defaultdict
from copy import deepcopy
from itertools import product

import axiom_rules
import fact_groups
import instantiate
import normalize
import options
import pddl
#from .pddl.conditions import NegatedAtom, Conjunction
import pddl_parser
import sas_tasks
import simplify
import timers
from constants import *
import tools
import variable_order

# TODO: The translator may generate trivial derived variables which are always
# true, for example if there ia a derived predicate in the input that only
# depends on (non-derived) variables which are detected as always true.
# Such a situation was encountered in the PSR-STRIPS-DerivedPredicates domain.
# Such "always-true" variables should best be compiled away, but it is
# not clear what the best place to do this should be. Similar
# simplifications might be possible elsewhere, for example if a
# derived variable is synonymous with another variable (derived or
# non-derived).

DEBUG = False

simplified_effect_condition_counter = 0
added_implied_precondition_counter = 0



def dump_task(init, goals, actions, axioms, axiom_layer_dict):
    old_stdout = sys.stdout
    with open("output.dump", "w") as dump_file:
        sys.stdout = dump_file
        print("Initial state")
        for atom in init:
            print(atom)
        print()
        print("Goals")
        for goal in goals:
            print(goal)
        for action in actions:
            print()
            print("Action")
            action.dump()
        for axiom in axioms:
            print()
            print("Axiom")
            axiom.dump()
        print()
        print("Axiom layers")
        for atom, layer in axiom_layer_dict.items():
            print("%s: layer %d" % (atom, layer))
    sys.stdout = old_stdout

def get_preconditions(preconditions):
    assert type(preconditions) == pddl.conditions.Conjunction, "Only support conjunctive preconditions"
    neg_prec = set()
    pos_prec = set()
    for cond in preconditions.parts:
        if type(cond) == pddl.conditions.NegatedAtom:
            neg_prec.add(cond.predicate)
        else:
            pos_prec.add(cond.predicate)
    return pos_prec, neg_prec

def get_set_from_conjunction(condition):
    assert type(condition) == pddl.conditions.Conjunction, "Not a conjunction"
    prop_set = set()
    for cond in condition.parts:
        prop_set.add(cond.predicate)
    return prop_set

def get_effects(effect_list):
    neg_eff = set()
    add_eff = set()
    for eff in effect_list:
        if type(eff.literal) == pddl.conditions.NegatedAtom:
            assert len(eff.literal.args) == 0, "Non proposition "+str(eff.literal)
            neg_eff.add(eff.literal.predicate)
        else:
            assert len(eff.literal.args) == 0, "Non proposition " + str(eff.literal)
            add_eff.add(eff.literal.predicate)
    return add_eff, neg_eff

def get_atom_set(prop_list):
    atom_set = set()
    for prop in prop_list:
        assert len(prop.args) == 0, "Non proposition " + str(prop)
        atom_set.add(prop.predicate)
    return atom_set

def get_pred_set(pred_list):
    atom_set = set()
    for prop in pred_list:
        if prop.name != "=":
            assert len(prop.arguments) == 0, "Non proposition " + str(prop)
            atom_set.add(prop.name)
    return atom_set


def parse_to_dict(problem):
    model_map = {}

    # Create the dictionary
    # TODO: Assuming its only propositions

    # Instance details
    model_map[INSTANCE] = {}
    model_map[INSTANCE][INIT] = get_atom_set(problem.init)
    model_map[INSTANCE][GOAL] = get_set_from_conjunction(problem.goal)

    # Domain details
    model_map[DOMAIN] = {}
    model_map[DOMAIN][PREDICATES] = get_pred_set(problem.predicates)
    model_map[DOMAIN][ACTIONS] = {}
    for act in problem.actions:
        #act.dump()
        model_map[DOMAIN][ACTIONS][act.name] = {}
        assert len(act.parameters) == 0, "Should be zero"
        model_map[DOMAIN][ACTIONS][act.name][PARAMETERS] = []
        model_map[DOMAIN][ACTIONS][act.name][POS_PREC] = set()
        model_map[DOMAIN][ACTIONS][act.name][NEG_PREC] = set()
        model_map[DOMAIN][ACTIONS][act.name][ADDS] = set()
        model_map[DOMAIN][ACTIONS][act.name][DELS] = set()
        pos_prec, neg_prec = get_preconditions(act.precondition)
        adds, dels = get_effects(act.effects)
        # print (act.name)
        # print(adds)
        model_map[DOMAIN][ACTIONS][act.name][ADDS] |= adds
        model_map[DOMAIN][ACTIONS][act.name][DELS] |= dels

        model_map[DOMAIN][ACTIONS][act.name][NEG_PREC] |= neg_prec

        model_map[DOMAIN][ACTIONS][act.name][POS_PREC] |= pos_prec


    #print (model_map[DOMAIN][ACTIONS]["dialogue-disambiguation-end_conversation_DETDUP_resolve-end_conversation-EQ-end_conversation-outcome-fallback__"])
    return model_map


def main():
    timer = timers.Timer()
    with timers.timing("Parsing", True):
        task = pddl_parser.open(
            domain_filename=options.domain, task_filename=options.task)
    task_map = parse_to_dict(task)
    import yaml
    with open('model.yaml', 'w') as m_fd:
        yaml.dump(task_map, m_fd)


if __name__ == "__main__":
    main()
