import tempfile
from flask import Flask, render_template, request, jsonify
from maiunsolv.driver import Driver
from flask_cors import CORS
import os, sys, pprint
app = Flask(__name__)
CORS(app)
#sk(__name__)

@app.route('/foil')
def foil():
    assert 'domain' in request.args
    fd, domain_file = tempfile.mkstemp()
    with open(domain_file, 'w') as d_fd:
        d_fd.write(request.args['domain'])
    assert 'problem' in request.args
    fd, problem_file = tempfile.mkstemp()
    with open(problem_file, 'w') as p_fd:
        p_fd.write(request.args['problem'])

    if 'storyboard' in request.args:
        fd, foil_file = tempfile.mkstemp()
        with open(foil_file, 'w') as f_fd:
            f_fd.write(request.args['foil'])
    else:
        foil_file = None


    driver_obj = Driver(domain_file, problem_file, foil_file)

    status = driver_obj.test_foil()

    
    result_map = {'STATUS':status}
    return jsonify(result_map)




@app.route('/explanation')
def explanation():
    assert 'domain' in request.args
    fd, domain_file = tempfile.mkstemp()
    with open(domain_file, 'w') as d_fd:
        d_fd.write(request.args['domain'])
    assert 'problem' in request.args
    fd, problem_file = tempfile.mkstemp()
    with open(problem_file, 'w') as p_fd:
        p_fd.write(request.args['problem'])

    if 'storyboard' in request.args:
        fd, foil_file = tempfile.mkstemp()
        with open(foil_file, 'w') as f_fd:
            f_fd.write(request.args['foil'])
    else:
        foil_file = None


    driver_obj = Driver(domain_file, problem_file, foil_file)

    status, preds, landmark, plan, failure_info = driver_obj.full_unsolvability_test()
    
    result_map = {'STATUS':status, 'PREDS': preds, 'PLAN': plan, 'LANDMARK':landmark, 'FAILURE': failure_info}
    return jsonify(result_map)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
