import re
import os
import yaml
import copy
import tempfile
from maiunsolv.constants import *

PARSER_CMD = "./run_parser.sh {} {}"

ACTION_TEMPLATE = "(:action {} \n:parameters ({}) \n :precondition (and {} )\n :effect (and {} ))"
NON_DET_ACTION_TEMPLATE = "(:action {} \n:parameters ({}) \n :precondition (and {} )\n :effect ({})"
LABELED_ONEOF_TEMPLATE = "(labeled-oneof {} {} )"
OUTCOME_TEMPLATE = "(outcome {} (and {} ))"

def get_observation_fct(action_name):
    return GENERATED_OBS_PREFIX + action_name

def read_model(domain_file, problem_file, foil):
    os.popen(PARSER_CMD.format(domain_file, problem_file)).read().strip()
    foil_fluents = set()
    with open(MODEL_YAML) as m_fd:
        model_dict = yaml.load(m_fd)
    if len(foil) > 0:
        model_dict[INSTANCE][GOAL].add(get_observation_fct(foil[-1]))
        model_dict[DOMAIN][PREDICATES].add(get_observation_fct(foil[-1]))
        foil_fluents.add(get_observation_fct(foil[-1]))
        for act in model_dict[DOMAIN][ACTIONS]:
            orig_act_name = act.split(ONEOFF)[0]
            if orig_act_name in foil:
                obs_index = foil.index(orig_act_name)
                model_dict[DOMAIN][ACTIONS][act][ADDS].add(get_observation_fct(foil[obs_index]))
                model_dict[DOMAIN][PREDICATES].add(get_observation_fct(foil[obs_index]))
                foil_fluents.add(get_observation_fct(foil[obs_index]))
                if obs_index > 0:
                    model_dict[DOMAIN][ACTIONS][act][POS_PREC].add(get_observation_fct(foil[obs_index-1]))
                    foil_fluents.add(get_observation_fct(foil[obs_index-1]))
                    model_dict[DOMAIN][PREDICATES].add(get_observation_fct(foil[obs_index-1]))
    return model_dict, foil_fluents

def get_list_of_props(prop_set):
    return ["("+prop+")" for prop in prop_set]

def get_list_of_neg_props(prop_set):
    return ["(not ("+prop+"))" for prop in prop_set]

def write_model(model, dst_domain_file, dst_problem_file):
    with open(DOMAIN_TEMPLATE_FILE) as d_fd:
        dom_templ_str = d_fd.read()

    with open(PROBLEM_TEMPLATE_FILE) as p_fd:
        prob_templ_str = p_fd.read()

    prob_str = prob_templ_str.format("\n".join(get_list_of_props(model[INSTANCE][INIT])), "\n".join(get_list_of_props(model[INSTANCE][GOAL])))
    action_denfs = []
    for act in model[DOMAIN][ACTIONS]:
        par_str = " ".join(model[DOMAIN][ACTIONS][act][PARARMETERS])
        prec_str = "\n".join(get_list_of_props(model[DOMAIN][ACTIONS][act][POS_PREC])) + "\n" \
                   + "\n".join(get_list_of_neg_props(model[DOMAIN][ACTIONS][act][NEG_PREC]))
        eff_str = "\n".join(get_list_of_props(model[DOMAIN][ACTIONS][act][ADDS])) + "\n" \
                  + "\n".join(get_list_of_neg_props(model[DOMAIN][ACTIONS][act][DELS]))
        action_denfs.append(ACTION_TEMPLATE.format(act, par_str, prec_str, eff_str))

    dom_str = dom_templ_str.format("\n".join(get_list_of_props(model[DOMAIN][PREDICATES])), "\n".join(action_denfs))

    with open(dst_domain_file, 'w') as d_fd:
        d_fd.write(dom_str)
    with open(dst_problem_file, 'w') as d_fd:
        d_fd.write(prob_str)


def write_minimal_model(model, dst_domain_file, dst_problem_file):
    with open(DOMAIN_TEMPLATE_FILE) as d_fd:
        dom_templ_str = d_fd.read()

    with open(PROBLEM_TEMPLATE_FILE) as p_fd:
        prob_templ_str = p_fd.read()

    prob_str = prob_templ_str.format("\n".join(get_list_of_props(model[INSTANCE][INIT])), "\n".join(get_list_of_props(model[INSTANCE][GOAL])))

    action_denfs = []
    for act in model[DOMAIN][ACTIONS]:
        if act != PREDICATES and len(model[DOMAIN][ACTIONS][act][ADDS]) > 0:

            par_str = " ".join(model[DOMAIN][ACTIONS][act][PARARMETERS])
            prec_str = "\n".join(get_list_of_props(model[DOMAIN][ACTIONS][act][POS_PREC])) + "\n" \
                       + "\n".join(get_list_of_neg_props(model[DOMAIN][ACTIONS][act][NEG_PREC]))
            eff_str = "\n".join(get_list_of_props(model[DOMAIN][ACTIONS][act][ADDS])) + "\n" \
                      + "\n".join(get_list_of_neg_props(model[DOMAIN][ACTIONS][act][DELS]))
            action_denfs.append(ACTION_TEMPLATE.format(act, par_str, prec_str, eff_str))

    dom_str = dom_templ_str.format("\n".join(get_list_of_props(model[DOMAIN][PREDICATES])), "\n".join(action_denfs))

    with open(dst_domain_file, 'w') as d_fd:
        d_fd.write(dom_str)

    with open(dst_problem_file, 'w') as d_fd:
        d_fd.write(prob_str)

def write_min_non_det_model(model, dst_domain_file, dst_problem_file):
    #print(model)
    with open(DOMAIN_TEMPLATE_FILE) as d_fd:
        dom_templ_str = d_fd.read()

    with open(PROBLEM_TEMPLATE_FILE) as p_fd:
        prob_templ_str = p_fd.read()

    prob_str = prob_templ_str.format("\n".join(get_list_of_props(model[INSTANCE][INIT])), "\n".join(get_list_of_props(model[INSTANCE][GOAL])))

    action_denfs = []
    for act in model[DOMAIN][ACTIONS]:
        par_str = " ".join(model[DOMAIN][ACTIONS][act][PARARMETERS])
        label_strings = []
        eff_present = False
        for lab_out in model[DOMAIN][ACTIONS][act][LABELED_ONEOF]:
            out_come_strings = []
            for indiv_out in model[DOMAIN][ACTIONS][act][LABELED_ONEOF][lab_out]:

                if len(model[DOMAIN][ACTIONS][act][LABELED_ONEOF][lab_out][indiv_out][ADDS]) > 0 or len(model[DOMAIN][ACTIONS][act][LABELED_ONEOF][lab_out][indiv_out][DELS]) > 0:
                    eff_present = True
                out_come_strings.append(OUTCOME_TEMPLATE.format(indiv_out,
                    "\n".join(get_list_of_props(model[DOMAIN][ACTIONS][act][LABELED_ONEOF][lab_out][indiv_out][ADDS])) + "\n" \
                      + "\n".join(get_list_of_neg_props(model[DOMAIN][ACTIONS][act][LABELED_ONEOF][lab_out][indiv_out][DELS]))))
            label_strings.append(LABELED_ONEOF_TEMPLATE.format(lab_out, "\n".join(out_come_strings)))


        if eff_present:
            prec_str = "\n".join(get_list_of_props(model[DOMAIN][ACTIONS][act][POS_PREC])) + "\n" \
                       + "\n".join(get_list_of_neg_props(model[DOMAIN][ACTIONS][act][NEG_PREC]))
            eff_str = "\n".join(label_strings)
            action_denfs.append(ACTION_TEMPLATE.format(act, par_str, prec_str, eff_str))

    dom_str = dom_templ_str.format("\n".join(get_list_of_props(model[DOMAIN][PREDICATES])), "\n".join(action_denfs))

    with open(dst_domain_file, 'w') as d_fd:
        d_fd.write(dom_str)

    with open(dst_problem_file, 'w') as d_fd:
        d_fd.write(prob_str)


def make_non_det_domain(model):
    non_det_model = {}
    non_det_model[INSTANCE] = {}
    non_det_model[INSTANCE][INIT] = copy.deepcopy(model[INSTANCE][INIT])
    non_det_model[INSTANCE][GOAL] = copy.deepcopy(model[INSTANCE][GOAL])
    non_det_model[DOMAIN] = {}
    non_det_model[DOMAIN][PREDICATES] = copy.deepcopy(model[DOMAIN][PREDICATES])
    non_det_model[DOMAIN][ACTIONS] = {}
    for act in model[DOMAIN][ACTIONS]:
        act_name = act.split(ONEOFF)[0]
        rest = act.split(ONEOFF)[1]
        oneoff_label = rest.split(OUTCOME)[0]
        outcome = rest.split(OUTCOME)[1]
        if act_name not in non_det_model[DOMAIN][ACTIONS]:
            non_det_model[DOMAIN][ACTIONS][act_name] = {}
            non_det_model[DOMAIN][ACTIONS][act_name][PARARMETERS] = copy.deepcopy(model[DOMAIN][ACTIONS][act][PARARMETERS])
            non_det_model[DOMAIN][ACTIONS][act_name][POS_PREC] = copy.deepcopy(model[DOMAIN][ACTIONS][act][POS_PREC])
            non_det_model[DOMAIN][ACTIONS][act_name][NEG_PREC] = copy.deepcopy(model[DOMAIN][ACTIONS][act][NEG_PREC])
        if LABELED_ONEOF not in non_det_model[DOMAIN][ACTIONS][act_name]:
            non_det_model[DOMAIN][ACTIONS][act_name][LABELED_ONEOF] = {}
        if oneoff_label not in non_det_model[DOMAIN][ACTIONS][act_name][LABELED_ONEOF]:
            non_det_model[DOMAIN][ACTIONS][act_name][LABELED_ONEOF][oneoff_label] = {}
        if outcome not in non_det_model[DOMAIN][ACTIONS][act_name][LABELED_ONEOF][oneoff_label]:
            non_det_model[DOMAIN][ACTIONS][act_name][LABELED_ONEOF][oneoff_label][outcome] = {}

        non_det_model[DOMAIN][ACTIONS][act_name][LABELED_ONEOF][oneoff_label][outcome][ADDS] = copy.deepcopy(model[DOMAIN][ACTIONS][act][ADDS])
        non_det_model[DOMAIN][ACTIONS][act_name][LABELED_ONEOF][oneoff_label][outcome][DELS] = copy.deepcopy(
            model[DOMAIN][ACTIONS][act][DELS])

    return non_det_model

